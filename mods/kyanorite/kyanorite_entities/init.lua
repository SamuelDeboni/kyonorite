local testDummy = {
    initial_properties = {
        hp_max = 10000,
        physical = true,
        collide_with_objects = false,
        collisionbox = {-0.5, -1, -0.5, 0.5, 1, 0.5},
        visual = "mesh",
        mesh = "kyanorite_entities_test_dummy.b3d",
        visual_size = {x = 10, y = 10},
        textures = {"kyanorite_entities_test_dummy.png"}
    },

    message = "Default message"
}

function testDummy:set_message(msg)
    self.message = msg
end


function testDummy:on_step(dtime)
    local pos      = self.object:get_pos()
    local pos_down = vector.subtract(pos, vector.new(0, 1, 0))

    local delta
    if minetest.get_node(pos_down).name == "air" then
        delta = vector.new(0, -1, 0)
    elseif minetest.get_node(pos).name == "air" then
        delta = vector.new(0, 0, 1)
    else
        delta = vector.new(0, 1, 0)
    end

    delta = vector.multiply(delta, dtime)

    self.object:move_to(vector.add(pos, delta))
end

function testDummy:on_punch(hitter)
    minetest.chat_send_player(hitter:get_player_name(), self.message)
end


minetest.register_entity("kyanorite_entities:test_dummy", testDummy)


local securityCore = {
    initial_properties = {
        hp_max = 20,
        physical = true,
        collide_with_objects = false,
        collisionbox = {-0.5, -1, -0.5, 0.5, 1, 0.5},
        visual = "mesh",
        mesh = "kyanorite_entities_security_core.b3d",
        visual_size = {x = 10, y = 10},
        textures = {"kyanorite_entities_security_core.png"}
    },

    message = "Default message"
}

function securityCore:set_message(msg)
    self.message = msg
end

function securityCore:on_punch(hitter)
    
end


minetest.register_entity("kyanorite_entities:security_core", securityCore)
