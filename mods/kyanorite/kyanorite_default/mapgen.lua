--
-- Aliases for map generators
--

minetest.register_alias("mapgen_stone", "kyanorite_world:stone")
minetest.register_alias("mapgen_dirt", "kyanorite_world:dirt")
minetest.register_alias("mapgen_dirt_with_grass", "kyanorite_world:grass")
minetest.register_alias("mapgen_water_source", "kyanorite_world:water_source")
minetest.register_alias("mapgen_river_water_source", "kyanorite_world:river_water_source")
minetest.register_alias("mapgen_lava_source", "kyanorite_world:lava_source")
minetest.register_alias("mapgen_gravel", "kyanorite_world:gravel")

--
-- Register ores
--

-- Mgv6


function kyanorite_default.register_biomes(upper_limit)

	-- Grassland

	minetest.register_biome({
		name = "grassland",
		node_top = "kyanorite_world:grass",
		depth_top = 1,
		node_filler = "kyanorite_world:dirt",
		depth_filler = 1,
		node_riverbed = "kyanorite_world:gravel",
		depth_riverbed = 2,
		y_max = upper_limit,
		y_min = 6,
		heat_point = 50,
		humidity_point = 35,
	})

	-- Underground

	minetest.register_biome({
		name = "underground",
		y_max = -113,
		y_min = -31000,
		heat_point = 50,
		humidity_point = 50,
	})

	minetest.register_biome({
		name = "grassland_ocean",
		node_top = "kyanorite_world:gravel",
		depth_top = 1,
		node_filler = "kyanorite_world:gravel",
		depth_filler = 3,
		node_riverbed = "kyanorite_world:gravel",
		depth_riverbed = 2,
		y_max = 3,
		y_min = -112,
		heat_point = 50,
		humidity_point = 35,
	})
end
--
-- Detect mapgen, flags and parameters to select functions
--

-- Get setting or default
local mgv7_spflags = minetest.get_mapgen_setting("mgv7_spflags") or
	"mountains, ridges, nofloatlands, caverns"

-- Get setting or default
-- Make global for mods to use to register floatland biomes
default.mgv7_floatland_level =
	minetest.get_mapgen_setting("mgv7_floatland_level") or 1280
default.mgv7_shadow_limit =
	minetest.get_mapgen_setting("mgv7_shadow_limit") or 1024

minetest.clear_registered_biomes()
minetest.clear_registered_ores()
minetest.clear_registered_decorations()

local mg_name = minetest.get_mapgen_setting("mg_name")


kyanorite_default.register_biomes(31000)
--default.register_ores()
