-- Opened chest definition
minetest.register_node("kyanorite_utils:wooden_chest_open", {
    description = "Wooden chest open",
    drawtype = "mesh",
    tiles = {"kyanorite_utils_wooden_chest.png"},
    mesh = "kyanorite_utils_wooden_chest_open.b3d",
    paramtype2 = "facedir",
    groups = {choppy = 3},
    sounds = default.node_sound_wood_defaults(),

    -- Node place callback
    after_place_node = function(pos, placer)
        
        local meta = minetest.get_meta(pos)

        -- Set formspec
        meta:set_string("formspec",
            "size[8,7]"..
            "list[context;main;0,0;8,2;]"..
            "list[current_player;main;0,3;8,4;]")
        
        -- Create metadata
        local inv = meta:get_inventory()
        inv:set_size("main",8*2)

        -- close the chest
        minetest.after(0.25, minetest.swap_node, pos, {
            name = "kyanorite_utils:wooden_chest",
            param2 = minetest.get_node(pos).param2 })
        
    end,

    
    on_receive_fields = function(pos, formname, fields, player)
        
        -- when the chest is closed
        if(fields.quit) then
            minetest.sound_play("default_chest_close", {gain = 0.2, pos = pos, max_hear_distance = 10})
            
            -- close the chest
            minetest.after(0.25, minetest.swap_node, pos, {
                name = "kyanorite_utils:wooden_chest",
                param2 = minetest.get_node(pos).param2 })
            return 
        end
        print(fields.x)
    end
})

-- wooden chest definition
minetest.register_node("kyanorite_utils:wooden_chest", {
    
    description = "Wooden chest",
    drawtype = "mesh",
    tiles = {"kyanorite_utils_wooden_chest.png"},
    mesh = "kyanorite_utils_wooden_chest.b3d",
    paramtype2 = "facedir",
    groups = {choppy = 3},
    sounds = default.node_sound_wood_defaults(),
    
    -- Node place callback
    after_place_node = function(pos, placer)
        local meta = minetest.get_meta(pos)
        
        -- Set formspec
        meta:set_string("formspec",
            "size[8,7]"..
            "list[context;main;0,0;8,2;]"..
            "list[current_player;main;0,3;8,4;]")

        -- Create metadata
            local inv = meta:get_inventory()
        inv:set_size("main",8*2)
        
    end,

    -- Open the chest
    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        minetest.swap_node(pos, {
            name = "kyanorite_utils:wooden_chest_open",
            param2 = node.param2 })
        minetest.sound_play("default_chest_open", {gain = 0.3, pos = pos, max_hear_distance = 10})
    end,

    on_receive_fields = function(pos, formname, fields, player)
        print(fields.x)
    end,

    -- The chest is only digable when the chest is empty
    can_dig = function(pos)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        
        if inv:is_empty("main") then
            return true
        else
            return false
        end
    end,

})