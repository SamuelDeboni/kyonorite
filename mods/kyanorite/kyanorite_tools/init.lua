
-- copper pickaxe
minetest.register_node("kyanorite_tools:copper_pickaxe",{
        description = "copper pickaxe",
        drawtype = "mesh",
        tiles = {"kyanorite_tools_copper_pickaxe.png"},
        mesh = "kyanorite_tools_basic_pickaxe.b3d",
        inventory_image = "kyanorite_tools_copper_pickaxe_icon.png",
        groups = {dig_immediate = 3},
        stack_max = 1,
        visual_scale = 0.5,
        tool_capabilities = {
            full_punch_interval = 1.5,
            max_drop_level = 4,
            groupcaps = {
                cracky = {
                    maxlevel = 3,
                    times = { [1]=2.6, [2]=2.4, [3]=2.2 }
                },
                crumbly = {
                    maxlevel = 3,
                    times = { [1]=2.0, [2]=1.80, [3]=1.60 }
                }
            },
            damage_groups = {fleshy=2},
        }

    })

-- copper axe
minetest.register_node("kyanorite_tools:copper_axe",{
        description = "copper axe",
        drawtype = "mesh",
        tiles = {"kyanorite_tools_copper_axe.png"},
        mesh = "kyanorite_tools_basic_axe.b3d",
        inventory_image = "kyanorite_tools_copper_axe_icon.png",
        groups = {dig_immediate = 3},
        stack_max = 1,
        visual_scale = 0.5,

        tool_capabilities = {
            full_punch_interval = 1.5,
            max_drop_level = 4,
            groupcaps = {
                choppy = {
                    maxlevel = 3,
                    times = { [1]=2.60, [2]=2.40, [3]=2.20 }
                },
            },
            damage_groups = {fleshy=2},
        },
        --on_use = function(itemstack, user, pointed_thing)
        --if user ~= nil then    
        --    local pos = user:get_pos()

        --    for i = -2,2 do
        --	local look = user:get_look_horizontal() - (i*0.5) 
        --	local pos2 = vector.new(math.cos(look)*2+pos.x,pos.y,math.sin(look)*2+pos.z) 

        --	ray = minetest.raycast(pos,pos2)

        --	minetest.chat_send_all("hit" .. ray:next().box_id)
        --    end
        --end
        --end,    
    })

-- steel pickaxe
minetest.register_node("kyanorite_tools:steel_pickaxe",{
        description = "steel pickaxe",
        drawtype = "mesh",
        tiles = {"kyanorite_tools_steel_pickaxe.png"},
        mesh = "kyanorite_tools_basic_pickaxe.b3d",
        inventory_image = "kyanorite_tools_steel_pickaxe_icon.png",
        groups = {dig_immediate = 3},
        stack_max = 1,
        visual_scale = 0.5,
        tool_capabilities = {
            full_punch_interval = 1.5,
            max_drop_level = 4,
            groupcaps = {
                cracky = {
                    maxlevel = 3,
                    times = { [1]=2.2, [2]=2.0, [3]=1.80 }
                },
                crumbly = {
                    maxlevel = 3,
                    times = { [1]=1.80, [2]=1.60, [3]=1.40 }
                }
            },
            damage_groups = {fleshy=2},
        }

    })

-- steel axe
minetest.register_node("kyanorite_tools:steel_axe",{
        description = "steel axe",
        drawtype = "mesh",
        tiles = {"kyanorite_tools_steel_axe.png"},
        mesh = "kyanorite_tools_basic_axe.b3d",
        inventory_image = "kyanorite_tools_steel_axe_icon.png",
        groups = {dig_immediate = 3},
        stack_max = 1,
        visual_scale = 0.5,

        tool_capabilities = {
            full_punch_interval = 1.5,
            max_drop_level = 4,
            groupcaps = {
                choppy = {
                    maxlevel = 3,
                    times = { [1]=1.20, [2]=1.00, [3]=0.60 }
                },
            },
            damage_groups = {fleshy=2},
        },
        --on_use = function(itemstack, user, pointed_thing)
        --	if user ~= nil then    
        --	    local pos = user:get_pos()
        --
        --	    for i = -2,2 do
        --		local look = user:get_look_horizontal() - (i*0.5) 
        --		local pos2 = vector.new(math.cos(look)*2+pos.x,pos.y,math.sin(look)*2+pos.z) 
        --
        --		ray = minetest.raycast(pos,pos2)
        --
        --		minetest.chat_send_all("hit" .. ray:next().box_id)
        --	    end
        --	end
        --    end,
    })

-- copper pickaxe crafting
minetest.register_craft({
        type = "shaped",
        output = "kyanorite_tools:copper_pickaxe",
        recipe = {
            {"default:copper_ingot","default:copper_ingot","default:copper_ingot"},
            {""		  			   ,"default:stick"       ,"" 					 },
            {""                    ,"default:stick"       ,""                    },
        }
    })

-- copper axe crafting
minetest.register_craft({
        type = "shaped",
        output = "kyanorite_tools:copper_axe",
        recipe = {
            {"default:copper_ingot","default:copper_ingot"},
            {"default:copper_ingot","default:stick"       },
            {"",                    "default:stick"       },
        }
    })

-- steel pickaxe crafting
minetest.register_craft({
        type = "shaped",
        output = "kyanorite_tools:steel_pickaxe",
        recipe = {
            {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
            {""		      ,"default:stick"      ,"" 		  },
            {""                   ,"default:stick"      ,""                   },
        }
    })

-- copper axe crafting
minetest.register_craft({
        type = "shaped",
        output = "kyanorite_tools:steel_axe",
        recipe = {
            {"default:steel_ingot","default:steel_ingot"},
            {"default:steel_ingot","default:stick"      },
            {"",                    "default:stick"     },
        }
    })
